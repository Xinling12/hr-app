import React, { Component } from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import userReducer from "./reducers/userReducer";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import Routes from "./components/App/Routes";
import { Layout } from "./components/Layout";
import { NavBar } from "./components/NavBar";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(userReducer, composeEnhancers(applyMiddleware(thunk, logger)));
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <NavBar />
        <Layout>
          <Routes />
        </Layout>
      </Provider>
    );
  }
}
