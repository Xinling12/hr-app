import { userConstants } from '../constants/userConstants';
import axios from 'axios';

export const registerRequest = (username) => {
    return {
        type: userConstants.REGISTER_REQUEST,
        payload: username
    };
};

export const registerSuccess = (data) => {
    return {
        type: userConstants.REGISTER_SUCCESS,
        payload: data
    };
};

export const registerFailure = (error) => {
    return {
        type: userConstants.REGISTER_FAILURE,
        payload: error
    };
};

export const loginRequest = (username) => {
    return {
        type: userConstants.LOGIN_REQUEST,
        payload: username
    };
};

export const loginSuccess = (data) => {
    return {
        type: userConstants.LOGIN_SUCCESS,
        payload: data
    };
};

export const loginFailure = (error) => {
    return {
        type: userConstants.LOGIN_FAILURE,
        payload: error
    };
};

// TODO: axios
export const register = (stateData) => {
    const {
        username,
        password,
        email,
        address,
        contactNumber,
        role,
        firstName,
        lastName,
        jobTitle,
        tfn,
        status,
        companyName,
        companyCode,
        companyDescription,
        abn
    } = stateData;
    return dispatch => {
        dispatch(registerRequest(username));
        if (role === 'individual') {// TODO host
            return axios.post('https://jr-hr-api-2.herokuapp.com/api/users/employees',
                {
                    username,
                    password,
                    email,
                    address,
                    contactNumber,
                    role,
                    firstName,
                    lastName,
                    jobTitle,
                    tfn,
                    status
                })
                .then(res => {
                    dispatch(registerSuccess(res.data));
                }, err => {
                    dispatch(registerFailure(err));
                });
        } else {// TODO company host
            return axios.post('https://jr-hr-api-2.herokuapp.com/api/users/companies',
                {
                    username,
                    password,
                    email,
                    address,
                    contactNumber,
                    role,
                    companyName,
                    companyCode,
                    companyDescription,
                    abn
                })
                .then(res => {
                    dispatch(registerSuccess(res.data));
                }, err => {
                    dispatch(registerFailure(err));
                });
        }

    };
};

// TODO host
export const login = (username, password) => {
    return dispatch => {
        dispatch(loginRequest(username));
        return axios.post('https://jr-hr-api-2.herokuapp.com/api/auth', { username, password })
            .then(res => {
                dispatch(loginSuccess(res.data));
            }, err => {
                dispatch(loginFailure(err));
            });
    };
};

export const logout = () => {
    return { type: userConstants.LOGOUT };
};
