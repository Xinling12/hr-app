import { userConstants } from '../constants/userConstants';

const defaultState = {
  id: null,
  username: null,
  role: null,
  token: null
};

const userReducer = (state = defaultState, action) => {
  switch (action.type) {
    case userConstants.REGISTER_SUCCESS:
      return Object.assign({}, action.payload);
    case userConstants.LOGIN_SUCCESS:
      return Object.assign({}, action.payload);
    case userConstants.LOGOUT:
      return Object.assign({}, defaultState);
    default:
      return state;
  }
};

export default userReducer;
