import React from "react";
import {Nav, Navbar} from "react-bootstrap";
import {FormControl} from "react-bootstrap";
import {Form} from "react-bootstrap";
import {Button} from "react-bootstrap";
import styled from "styled-components";
import {faUserCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Styles = styled.div `
  .navbar {
    background-color: #6286d3;
  }
  .navbar-nav .nav-link {
    margin-right: 20px;
  }

  .user-btn {
    cursor: pointer;
    margin-left: 10px;
  }

  #dropdown-basic-button {
    margin-left: 15px;
    margin-right: 40px;
  }

`;

export const NavBar = () => (
    <Styles>
        <Navbar bg="primary" variant="dark">
            <Navbar.Brand href="/">Home Page</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href="/companyprofile">Company Profile</Nav.Link>
                <Nav.Link href="/personalpage">Personal Profile</Nav.Link>
                <Nav.Link href="/employeelist">Employee List</Nav.Link>
            </Nav>
            <Form className="mr-sm-8" inline>
                <FormControl className="mr-sm-2" placeholder="Search..." type="text"/>
                <Button variant="outline-light">Search</Button>
                <Nav className="mr-auto">
                    <Nav.Link href="/login"><FontAwesomeIcon icon={faUserCircle}/>Log in</Nav.Link>
                </Nav>
                {/* <Button className="user-btn mr-sm-auto" variant="outline-light"><FontAwesomeIcon icon={faUserCircle}/>
                    Log in
                </Button> */}
                {/* <DropdownButton id="dropdown-basic-button" title="Log in">
                    <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                    <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                    <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                </DropdownButton> */} </Form>
        </Navbar>
    </Styles>
);
