import React from 'react';
import MaterialTable from 'material-table';

export default function EmployeeList() {
    const [state, setState] = React.useState({
        columns: [
            { title: 'ID', field: 'id' },
            { title: 'First Name', field: 'name' },
            { title: 'Surname', field: 'surname' },
            { title: 'Position', field: 'position', lookup: { 10: 'Finance', 20: 'HR', 30: 'Procurement'}},
            { title: 'E-mail', field: 'email'},
            { title: 'Phone', field: 'phone'},
            { title: 'Status', field: 'status', lookup: { 10: 'Pending', 20: 'Active', 30: 'Deactive'}},
        ],
        data: [
            { id: '0', name: 'Mehmet', surname: 'Baran', position: 10, email: 'test@123.com', phone: '0432843927', status: '10'},
            { id: '1', name: 'Eneks', surname: 'Fadk', position: 20, email: 'test@123.com', phone: '0432843927', status: '10'},
            { id: '2', name: 'Eshmet', surname: 'Tran', position: 30, email: 'test@123.com', phone: '0432843927', status: '30' },
            { id: '3', name: 'Mahfet', surname: 'ctran', position: 10, email: 'test@123.com', phone: '0432843927', status: '20' },
            
        ],
    });

    return (
        <MaterialTable
            title="Staff List"
            columns={state.columns}
            data={state.data}
            editable={{
                onRowAdd: newData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            const data = [...state.data];
                            data.push(newData);
                            setState({ ...state, data });
                        }, 600);
                    }),
                onRowUpdate: (newData, oldData) =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            const data = [...state.data];
                            data[data.indexOf(oldData)] = newData;
                            setState({ ...state, data });
                        }, 600);
                    }),
                onRowDelete: oldData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            const data = [...state.data];
                            data.splice(data.indexOf(oldData), 1);
                            setState({ ...state, data });
                        }, 600);
                    }),
            }}
        />
    );
}

















// import React, { Component } from "react";
// import { Container, Row, Col } from "reactstrap";
// import ModalForm from "./Modals/Modal";
// import DataTable from "./Tables/DataTable";
// import { CSVLink } from "react-csv";

// class EmployeeList extends Component {
//   state = {
//     items: []
//   };

//   getItems() {
//     fetch("http://localhost:3000/crud")
//       .then(response => response.json())
//       .then(items => this.setState({ items }))
//       .catch(err => console.log(err));
//   }

//   addItemToState = item => {
//     this.setState(prevState => ({
//       items: [...prevState.items, item]
//     }));
//   };

//   updateState = item => {
//     const itemIndex = this.state.items.findIndex(data => data.id === item.id);
//     const newArray = [
//       // destructure all items from beginning to the indexed item
//       ...this.state.items.slice(0, itemIndex),
//       // add the updated item to the array
//       item,
//       // add the rest of the items to the array from the index after the replaced item
//       ...this.state.items.slice(itemIndex + 1)
//     ];
//     this.setState({ items: newArray });
//   };

//   deleteItemFromState = id => {
//     const updatedItems = this.state.items.filter(item => item.id !== id);
//     this.setState({ items: updatedItems });
//   };

//   componentDidMount() {
//     this.getItems();
//   }

//   render() {
//     return (
//       <Container className="App">
//         <Row>
//           <Col>
//             <DataTable
//               items={this.state.items}
//               updateState={this.updateState}
//               deleteItemFromState={this.deleteItemFromState}
//             />
//           </Col>
//         </Row>
//         <Row>
//           <Col>
//             <CSVLink
//               filename={"db.csv"}
//               color="primary"
//               style={{ float: "left", marginRight: "10px" }}
//               className="btn btn-primary"
//               data={this.state.items}
//             >
//               Download CSV
//             </CSVLink>
//             <ModalForm
//               buttonLabel="Add New Employee"
//               addItemToState={this.addItemToState}
//             />
//           </Col>
//         </Row>
//       </Container>
//     );
//   }
// }

// export default EmployeeList;
