import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Home } from './../Home';
import { CompanyProfile } from './../CompanyProfile';
import EmployeeList from '../EmployeeList';
import PersonalPage from '../PersonalPage';
import Login from '../Login/LoginForm';
import Logout from '../Login/LogoutButton';
import Register from '../Register/RegisterForm';

export default () => (

    <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/logout" component={Logout} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/companyprofile" component={CompanyProfile} />
        <Route exact path="/employeelist" component={EmployeeList} />
        <Route exact path="/personalpage" component={PersonalPage} />
    </Switch >

);
