import React from "react";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import ListGroupItem from "react-bootstrap/ListGroupItem";

export const CompanyProfile = () => (
  <div>
    <Card border="primary" style={{ width: "100%", marginTop: "40px" }}>
      <Card.Header>About us</Card.Header>
      <Card.Body>
        <Card.Title>Primary Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
      </Card.Body>
    </Card>
    <Card border="primary" style={{ width: "100%", marginTop: "40px" }}>
      <Card.Header>Department Contact Info</Card.Header>
      <Card.Body>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
      </Card.Body>
      <ListGroup className="list-group-flush">
        <ListGroupItem>Finance: 12938483</ListGroupItem>
        <ListGroupItem>HR: 3829495</ListGroupItem>
        <ListGroupItem>Procument: 472989475</ListGroupItem>
      </ListGroup>
    </Card>
  </div>
);
