import React, {Component} from "react";
import {
    Card,
    CardBody,
    CardHeader,
    CardText,
    Row,
    Col
} from "reactstrap";
import {Button} from "reactstrap";


class PersonalPage extends Component {
    state = {
        items: []
    };

    getItems() {
        fetch("http://localhost:3000/crud").then(response => response.json()).then(items => this.setState({items})).catch(err => console.log(err));
    }

    updateState = item => {
        const itemIndex = this.state.items.findIndex(data => data.id === item.id);
        const newArray = [
            // destructure all items from beginning to the indexed item
            ...this.state.items.slice(0, itemIndex),
            // add the updated item to the array
            item,
            // add the rest of the items to the array from the index after the replaced item
            ...this.state.items.slice(itemIndex + 1)
        ];
        this.setState({items: newArray});
    };

    componentDidMount() {
        this.getItems();
    }

    render() {
        return (
            <Row>
                <Col sm="4"
                    style={
                        {marginTop: "40px"}
                }>
                    <Card className="text-center" color="primary" outline>
                        <CardHeader>Name: Mehmet</CardHeader>
                        <CardBody>
                            <CardText>Address</CardText>
                            <CardText>Phone number</CardText>
                            <Button herf="#">Edit</Button>
                        </CardBody>

                    </Card>
                </Col>
                <Col sm="8"
                    style={
                        {marginTop: "40px"}
                }>
                    <Card body color="primary" outline>
                        <CardText>Position:</CardText>
                        <CardText>Email:</CardText>
                        <CardText>Annual leave summary:</CardText>
                        <CardText>Start date:</CardText>
                        <CardText>Status:</CardText>

                    </Card>
                </Col>
            </Row>
        );
    };

}


export default PersonalPage;
