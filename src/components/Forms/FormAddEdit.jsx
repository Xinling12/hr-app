import React from "react";
import {Button, Form, FormGroup, Label, Input} from "reactstrap";

class AddEditForm extends React.Component {
    state = {
        id: 0,
        first: "",
        last: "",
        position: "",
        email: "",
        address: "",
        phone: "",
        tfn: "",
        status: ""
    };

    onChange = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    submitFormAdd = e => {
        e.preventDefault();
        fetch("http://localhost:3000/crud", {
            method: "post",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(
                {
                    first: this.state.first,
                    last: this.state.last,
                    position: this.state.position,
                    email: this.state.email,
                    address: this.state.addresss,
                    phone: this.state.phone,
                    tfn: this.state.tfn,
                    status: this.state.status
                }
            )
        }).then(response => response.json()).then(item => {
            if (Array.isArray(item)) {
                this.props.addItemToState(item[0]);
                this.props.toggle();
            } else {
                console.log("failure");
            }
        }).catch(err => console.log(err));
    };

    submitFormEdit = e => {
        e.preventDefault();
        fetch("http://localhost:3000/crud", {
            method: "put",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(
                {
                    id: this.state.id,
                    first: this.state.first,
                    last: this.state.last,
                    position: this.state.position,
                    email: this.state.email,
                    address: this.state.addresss,
                    phone: this.state.phone,
                    tfn: this.state.tfn,
                    status: this.state.status
                }
            )
        }).then(response => response.json()).then(item => {
            if (Array.isArray(item)) { // console.log(item[0])
                this.props.updateState(item[0]);
                this.props.toggle();
            } else {
                console.log("failure");
            }
        }).catch(err => console.log(err));
    };

    componentDidMount() { // if item exists, populate the state with proper data
        if (this.props.item) {
            const {
                id,
                first,
                last,
                position,
                email,
                phone,
                status,
                address,
                tfn
            } = this.props.item;
            this.setState({
                id,
                first,
                last,
                position,
                email,
                phone,
                status,
                address,
                tfn
            });
        }
    }

    render() {
        return (
            <Form onSubmit={
                this.props.item ? this.submitFormEdit : this.submitFormAdd
            }>
                <FormGroup>
                    <Label for="first">First Name</Label>
                    <Input type="text" name="first" id="first"
                        onChange={
                            this.onChange
                        }
                        value={
                            this.state.first === null ? "" : this.state.first
                        }/>
                </FormGroup>
                <FormGroup>
                    <Label for="last">Last Name</Label>
                    <Input type="text" name="last" id="last"
                        onChange={
                            this.onChange
                        }
                        value={
                            this.state.last === null ? "" : this.state.last
                        }/>
                </FormGroup>
                <FormGroup>
                    <Label for="position">Position</Label>
                    <Input type="text" name="position" id="position"
                        onChange={
                            this.onChange
                        }
                        value={
                            this.state.position === null ? "" : this.state.position
                        }
                        placeholder="Job Title"/>
                </FormGroup>
                <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" name="email" id="email"
                        onChange={
                            this.onChange
                        }
                        value={
                            this.state.email === null ? "" : this.state.email
                        }/>
                </FormGroup>
                <FormGroup>
                    <Label for="email">Address</Label>
                    <Input type="address" name="address" id="address"
                        onChange={
                            this.onChange
                        }
                        value={
                            this.state.address === null ? "" : this.state.address
                        }/>
                </FormGroup>
                <FormGroup>
                    <Label for="phone">Phone</Label>
                    <Input type="text" name="phone" id="phone"
                        onChange={
                            this.onChange
                        }
                        value={
                            this.state.phone === null ? "" : this.state.phone
                        }
                        placeholder="ex. 555-555-5555"/>
                </FormGroup>
                <FormGroup>
                    <Label for="tfn">TFN</Label>
                    <Input type="text" name="tfn" id="tfn"
                        onChange={
                            this.onChange
                        }
                        value={
                            this.state.tfn === null ? "" : this.state.tfn
                        }
                        placeholder="TFN number"/>
                </FormGroup>
                <FormGroup>
                    <Label for="startDate">Start Date</Label>
                    <Input id="startDate" name="date" placeholder="date placeholder" type="date"/>
                </FormGroup>
                <FormGroup>
                    <Label for="leaveDate">Leave Date</Label>
                    <Input id="leaveDate" name="date" placeholder="date placeholder" type="date"/>
                </FormGroup>
                <FormGroup>
                    <Label for="status">Status</Label>
                    <Input id="status" name="select" type="select">
                        <option>Pending</option>
                        <option>Active</option>
                        <option>Deactive</option>
                    </Input>
                </FormGroup>
                <Button>Submit</Button>
            </Form>
        );
    }
}

export default AddEditForm;
