import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../../actions/userActions';
import { Button } from 'react-bootstrap';

const LogoutButton = (props) => {
  const logoutHandler = (e) => {
    e.preventDefault();
    props.dispatch(logout());
  };

  return (
    <Button variant="danger" onClick={logoutHandler}>Logout</Button>
  );
};

const mapStateToProps = (state) => {
  return {
    id: state.id,
    username: state.username,
    role: state.role,
    token: state.token
  }
}

const LogoutButtonC = connect(mapStateToProps)(LogoutButton);

export default LogoutButtonC;
