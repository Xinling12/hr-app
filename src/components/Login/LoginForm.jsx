import React, { Component } from 'react';
import { connect } from 'react-redux';
import { login } from '../../actions/userActions';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    // TODO: Check the data format
    onLoginFormSubmit = (e) => {
        e.preventDefault();
        this.props.dispatch(login(this.state.username, this.state.password));
    };

    onUsernameChange = (e) => {
        this.setState({ username: e.target.value });
    };

    onPasswordChange = (e) => {
        this.setState({ password: e.target.value });
    };

    render() {
        return (
            <form onSubmit={this.onLoginFormSubmit}>
                <label htmlFor="username" >
                    Username
          <input type="text" name="username" value={this.state.username} onChange={this.onUsernameChange} />
                </label>
                <label htmlFor="password" >
                    Password
          <input type="password" name="password" value={this.state.password} onChange={this.onPasswordChange} />
                </label>
                <input type="submit" value="Login" />
            </form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        id: state.id,
        username: state.username,
        role: state.role,
        token: state.token
    }
}

const LoginFormC = connect(mapStateToProps)(LoginForm);

export default LoginFormC;
