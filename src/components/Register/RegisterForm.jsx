import React, { Component } from 'react';
import { connect } from 'react-redux';
import { register } from '../../actions/userActions';
import { Button } from 'react-bootstrap';
import { RegisterCommon } from './RegisterCommon';
import { RegisterIndividual } from './RegisterIndividual';
import { RegisterCompany } from './RegisterCompany';

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      confirmPassword: "",
      email: "",
      address: "",
      contactNumber: "",
      role: "individual",
      firstName: "",
      lastName: "",
      jobTitle: "",
      tfn: "", // TODO: model impl
      status: "pending",
      companyName: "",
      companyCode: "",
      companyDescription: "",
      abn: "" // TODO: model impl
    };
  }

  // TODO: prevent wrong data
  onRegisterFormSubmit = (e) => {
    e.preventDefault();
    if (this.state.password !== this.state.confirmPassword) {
      console.log('password dismatch');
    }
    else { // TODO: check role for register dispatch
      this.props.dispatch(register(this.state));
    }
  };

  commonFieldHandlers = {// collection of common handlers
    onUsernameChange: (e) => {
      this.setState({ username: e.target.value });
    },
    onPasswordChange: (e) => {
      this.setState({ password: e.target.value });
    },
    onConfirmPasswordChange: (e) => {
      this.setState({ confirmPassword: e.target.value });
    },
    onEmailChange: (e) => {
      this.setState({ email: e.target.value });
    },
    onAddressChange: (e) => {
      this.setState({ address: e.target.value });
    },
    onContactNumberChange: (e) => {
      this.setState({ contactNumber: e.target.value });
    }
  }

  individualFieldHandlers = {// collection of individual handlers
    onFirstNameChange: (e) => {
      this.setState({ firstName: e.target.value });
    },
    onLastNameChange: (e) => {
      this.setState({ lastName: e.target.value });
    },
    onJobTitleChange: (e) => {
      this.setState({ jobTitle: e.target.value });
    },
    onTfnChange: (e) => {
      this.setState({ tfn: e.target.value });
    }
  }

  companyFieldHandlers = {// collection of company handlers
    onCompanyNameChange: (e) => {
      this.setState({ companyName: e.target.value });
    },
    onCompanyCodeChange: (e) => {
      this.setState({ companyCode: e.target.value });
    },
    onCompanyDescriptionChange: (e) => {
      this.setState({ companyDescription: e.target.value });
    },
    onAbnChange: (e) => {
      this.setState({ abn: e.target.value });
    }
  }

  isIndividual = () => {
    this.setState({
      role: "individual",
      status: "pending"
    });
  };

  isCompany = () => {
    this.setState({
      role: "company",
      status: "active"
    });
  };

  render() {
    let differentFileds;
    if (this.state.role === "individual") {
      differentFileds = <RegisterIndividual state={this.state} individualFieldHandlers={this.individualFieldHandlers} />;
    } else {
      differentFileds = <RegisterCompany state={this.state} companyFieldHandlers={this.companyFieldHandlers} />;
    }
    return (
      <form onSubmit={this.onRegisterFormSubmit}>
        <div className="selectRole">
          <Button variant={`${this.state.role === "individual" ? "" : "outline-"}primary`} onClick={this.isIndividual} >Individual</Button>
          <Button variant={`${this.state.role === "company" ? "" : "outline-"}dark`} onClick={this.isCompany} >Company</Button>
        </div>
        <RegisterCommon state={this.state} commonFieldHandlers={this.commonFieldHandlers} />
        {differentFileds}
        <input type="submit" value="Register" />
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.id,
    username: state.username,
    role: state.role,
    token: state.token
  }
}

const RegisterFormC = connect(mapStateToProps)(RegisterForm);

export default RegisterFormC;