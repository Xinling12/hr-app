import React from 'react';

export const RegisterCompany = (props) => {
    const { state, companyFieldHandlers } = props;

    return (
        <React.Fragment>
            <label htmlFor="companyName">
                Company Name
        <input type="text" name="companyName" value={state.companyName} onChange={companyFieldHandlers.onCompanyNameChange} />
            </label>
            <label htmlFor="companyCode">
                Company Code
        <input type="text" name="companyCode" value={state.companyCode} onChange={companyFieldHandlers.onCompanyCodeChange} />
            </label>
            <label htmlFor="companyDescription">
                Company Description
        <input type="text" name="companyDescription" value={state.companyDescription} onChange={companyFieldHandlers.onCompanyDescriptionChange} />
            </label>
            <label htmlFor="abn">
                ABN
        <input type="text" name="abn" value={state.abn} onChange={companyFieldHandlers.onAbnChange} />
            </label>
        </React.Fragment>
    );
};
