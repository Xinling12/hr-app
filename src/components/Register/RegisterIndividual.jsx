import React from 'react';

export const RegisterIndividual = (props) => {
  const { state, individualFieldHandlers } = props;

  return (
    <React.Fragment>
      <label htmlFor="firstName">
        First Name
        <input type="text" name="firstName" value={state.firstName} onChange={individualFieldHandlers.onFirstNameChange} />
      </label>
      <label htmlFor="lastName">
        Last Name
        <input type="text" name="lastName" value={state.lastName} onChange={individualFieldHandlers.onLastNameChange} />
      </label>
      <label htmlFor="jobTitle">
        Job Title
        <input type="text" name="jobTitle" value={state.jobTitle} onChange={individualFieldHandlers.onJobTitleChange} />
      </label>
      <label htmlFor="tfn">
        TFN
        <input type="text" name="tfn" value={state.tfn} onChange={individualFieldHandlers.onTfnChange} />
      </label>
    </React.Fragment>
  );
}; 