import React from 'react';

export const RegisterCommon = (props) => {
    const { state, commonFieldHandlers } = props;

    return (
        <React.Fragment>
            <label htmlFor="username" >
                Username
        <input type="text" name="username" value={state.username} onChange={commonFieldHandlers.onUsernameChange} />
            </label>
            <label htmlFor="password" >
                Password
        <input type="password" name="password" value={state.password} onChange={commonFieldHandlers.onPasswordChange} />
            </label>
            <label htmlFor="confirmPassword" >
                Confirm Password
        <input type="password" name="confirmPassword" value={state.confirmPassword} onChange={commonFieldHandlers.onConfirmPasswordChange} />
            </label>
            <label htmlFor="email">
                Email
        <input type="text" name="email" value={state.email} onChange={commonFieldHandlers.onEmailChange} />
            </label>
            <label htmlFor="address">
                Address
        <input type="text" name="address" value={state.address} onChange={commonFieldHandlers.onAddressChange} />
            </label>
            <label htmlFor="contactNumber">
                Contact Number
        <input type="text" name="contactNumber" value={state.contactNumber} onChange={commonFieldHandlers.onContactNumberChange} />
            </label>
        </React.Fragment>
    );
};
